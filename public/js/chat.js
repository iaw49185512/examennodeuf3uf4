$(document).ready(function () {
  //Inicializa socket con IO
  const socket = io();
  $('#selectRoom').on("change",()=>{
    var sala = $(this).find("option:selected").val();
    sessionStorage.setItem("canal", sala);
    socket.emit("changeChannel", $('#selectRoom').val());
    window.history.pushState(null,"Chat","/chat/"+sala);
    console.log("cambiando canal");
    $(this).val(sala);
  })
  
  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    var user = $("#autor").val();
    var sala = $('#selectRoom').find(":selected").val();
    $("#chatBox").append(`<p>${user}: ${msg}<p>`);
    var toSend = {user: user, text: msg};
    socket.emit("newMsg", toSend);
  });

  //Acciones a realizar cuando se detecta actividad en el canal newMsg
  socket.on("newMsg", (data) => {
    console.log(data);
    $("#chatBox").append(`<p>${data.user}: ${data.text}<p>`);
  })

  //Acciones a realizar cuando se detecta actividad en el canal changeChannel
  socket.on("changeChannel", function(channel) {
    console.log("cambiando cnaal");
    $("#chatBox").html('').append(`<p>Bienvenido al canal ${channel}<p>`)
  })
});