var mongoose = require("mongoose"),
    Docente = require("../models/docente");
exports.listarDocentes = async (req, res, next) => {
    try {
        var result = await Docente.find({});
        if (req.isApi) {
            console.log(res.status(200).jsonp(result));
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log('No ha encontrado ningun docente');
    }
};

exports.getDocenteById = async (req, res, next) => {
    console.log(req.params.id);
    try {
        var result = await Docente.find({"nombre": req.params.id});

        if (req.isApi) {
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log('No ha encontrado ningun docente con ese ID');
    }
};

exports.addDocente = (req, res, next) => {
    return res.status(200).send({
        id: 2,
        nombre: "Jacinto",
        apellido: "Verla"
    });
};

exports.updateDocente = (req, res, next) => {
    return res.status(200).send({
        id: 3,
        nombre: "Erik",
        apellido: "Pepe"
    });
};

exports.deleteDocente = (req, res, next) => {
    return res.status(200).send({
        id: 3,
        nombre: "Carlos",
        apellido: "Busto"
    })
}