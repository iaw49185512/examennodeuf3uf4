var mongoose = require("mongoose"),
    Asignatura = require("../models/asignatura");
exports.listAsignaturas = async (req, res, next) => {
    
    try {
        var result = await Asignatura.find({});
        if (req.isApi) {
            console.log(res.status(200).jsonp(result));
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log('No ha encontrado ninguna asignatura');
    }
   
}

exports.addAsignatura = (req, res, next) => {
    var newAsignatura = new Asignatura(req);
    newAsignatura.save((err, res) => {
        if (err) console.log(err);
        console.log("Asignatura insertada correctamente");
        return res;
    })
}

exports.updateAsignatura = (req, res, next) => {
    return res.status(200).send(  {
        id: 1,
        nombre: "Programacion",
        numHoras: 8,
        docente: {
            rel: "docente",
            method: "GET",
            href: "http://localhost:3000/api/docentes/2"
        },
        alumnos: [
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/2"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/1"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/3"
            }
        ]
    }
   );
}

exports.deleteAsignatura = (req, res, next) => {
    return res.status(200).send(  {
        id: 1,
        nombre: "Programacion",
        numHoras: 8,
        docente: {
            rel: "docente",
            method: "GET",
            href: "http://localhost:3000/api/docentes/2"
        },
        alumnos: [
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/2"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/1"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/3"
            }
        ]
    }
   );
}