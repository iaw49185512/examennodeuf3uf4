var mongoose = require("mongoose"),
    Alumno = require("../models/alumnos");
exports.listarAlumnos = async(req, res, next) => {
    try {
        var result = await Alumno.find({});
        if (req.isApi) {
            console.log(res.status(200).jsonp(result));
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log('No ha encontrado ningun alumno');
    }
};

exports.getAlumnoById = async(req, res, next) => {
    console.log(req.params.id);
    try {
        var result = await Alumno.find({"nombre": req.params.id});

        if (req.isApi) {
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log('No ha encontrado ningun alumno con ese ID');
    }
};

// Añadir alumno
exports.addAlumno = (req, res, next) => {
    var newAlumno = new Alumno(req);
    newAlumno.save((err, res) => {
        if(err) console.log(err);
        console.log("Insertado en la DB");
        return res;
    });
};

// Update alumno
exports.updateAlumno = (req, res, next) => {
    return res.status(200).send(  {
        nombre: "Josefa",
        apellido: "Lavanda"
     }
   );
};

// Delete alumno
exports.deleteAlumno = (req, res, next) => {
    return res.status(200).send({
        nombre: "Pau",
        apellido: "Bosch"

    });
}