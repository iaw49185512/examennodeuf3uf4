var mongoose = require("mongoose"),
    Chat = require("../models/chats");
exports.newMsg = (req, res, next) =>{
    var newMsg = new Chat(req);
    newMsg.save((err, res) => {
        if(err) console.log(err);
        console.log("Insertado en la DB");
        return res;
    })
}

exports.listMsgs = async (req, res, next) =>{
    try {
        var result = await Chat.find({});
        if (req.isApi) {
            console.log(res.status(200).jsonp(result));
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log("No se ha podido cargar el chat");
    }
}