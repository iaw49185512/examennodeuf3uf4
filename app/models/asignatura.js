var mongoose = require("mongoose");
    Schema = mongoose.Schema;

var AsignaturaSchema = new Schema({
    nombre: {type: String},
    numHoras: {type: String},
    docente: {type: Schema.ObjectId},
    alumnos: {type: [Schema.ObjectId]},
    create_at: {type: Date, default: Date.new}
});

module.exports = mongoose.model("Asignatura", AsignaturaSchema);