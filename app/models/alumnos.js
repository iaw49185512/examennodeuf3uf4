var mongoose = require("mongoose");
    Schema = mongoose.Schema;

var alumnoSchema = new Schema({
    nombre: {type: String},
    apellido: {type: String},
});

module.exports = mongoose.model("Alumno", alumnoSchema);