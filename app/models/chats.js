var mongoose = require("mongoose");
    Schema = mongoose.Schema;

var chatSchema = new Schema({
    user: {type: String},
    msg: {type: String},
    canal: {type: String}
});

module.exports = mongoose.model("Chats", chatSchema);