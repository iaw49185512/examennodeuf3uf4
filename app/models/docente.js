var mongoose = require("mongoose");
    Schema = mongoose.Schema;

var docenteSchema = new Schema({
    nombre: {type: String},
    apellido: {type: String},
});

module.exports = mongoose.model("Docente", docenteSchema);