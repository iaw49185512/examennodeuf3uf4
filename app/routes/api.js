var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
var body_parser = require('body-parser');
var app = express();
app.use(body_parser.urlencoded({extended:true}));

// Alumnos Controller
var ctrlAlumnos = require(path.join(ctrlDir, "alumnos"));
// Docentes Controller
var ctrlDocentes = require(path.join(ctrlDir, "docentes"));
// Asignatura Controller
var ctrlAsignaturas = require(path.join(ctrlDir, "asignaturas"));
// Chat Controller
var ctrlChats = require(path.join(ctrlDir, "chats"));

// We use this middlewere to define the request as API type
router.use(function timeLog(req, res, next) {
    req.isApi = true;
    next();
});

// Rutas alumno
router.get("/alumnos", ctrlAlumnos.listarAlumnos);
router.get("/alumnos/:id", ctrlAlumnos.getAlumnoById);
router.post("/alumnos", ctrlAlumnos.addAlumno);
router.put("/alumnos/:id", ctrlAlumnos.updateAlumno);
router.delete("/alumnos/:id", ctrlAlumnos.deleteAlumno);

// Rutas docentes
router.get("/docentes", ctrlDocentes.listarDocentes);
router.get("/docentes/:id", ctrlDocentes.getDocenteById);
router.post("/docentes", ctrlDocentes.addDocente);
router.put("/docentes/:id", ctrlDocentes.updateDocente);
router.delete("/docentes/:id", ctrlDocentes.deleteDocente);

// Rutas asignaturas
router.get("/asignaturas", ctrlAsignaturas.listAsignaturas);
router.post("/asignatura/save", ctrlAsignaturas.addAsignatura);
router.put("/asignaturas/:id", ctrlAsignaturas.updateAsignatura);
router.delete("/asignaturas/:id", ctrlAsignaturas.deleteAsignatura);

//Rutas chat
router.post("/newMsg", ctrlChats.newMsg);
router.get("/chat/:id", ctrlChats.listMsgs);

module.exports = router;