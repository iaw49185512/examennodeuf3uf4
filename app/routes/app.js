var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();

var alumnoCtrl = require(path.join(ctrlDir, "alumnos"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));
var asignaturaCtrl = require(path.join(ctrlDir, "asignaturas"));

router.get("/", function(req,res,next) {
    res.render("chat", {title:"Express"});
});

router.get("/asignatura/new", async function(req, res, next) {
    var docente = await docenteCtrl.listarDocentes(req, res, next);
    var alumnos = await alumnoCtrl.listarAlumnos(req, res, next);
    res.render("newAsignatura", {docentes: docente, alumnos: alumnos});
});

router.post("/asignatura/save", (req, res) => {
    console.log(req.body);
    asignaturaCtrl.addAsignatura(req.body);
    res.send("Asignatura introducida correctamente");
})

module.exports = router;