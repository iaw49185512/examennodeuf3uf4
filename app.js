require("dotenv").config();
const port = process.env.SERVER_PORT || 6000;
var express = require("express"),
  app = express(),
  server = require("http").createServer(app),
  path = require("path");
var mongoose = require("mongoose");
var hateoasLinker = require('express-hateoas-links');
var body_parser = require("body-parser");

var io = require("socket.io")(server);

server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});
//Connect to mongodb://devroot:devroot@mongo:27017/chat?authSource=admin
mongoose.connect(
  `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
  { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
  }
);

// Import routes of our app

var routes = require("./app/routes/app");
var apiRoutes = require("./app/routes/api");
var handlerError = require("./app/routes/handler");

// view engine setup and other configurations
app.set("views", path.join(__dirname,"app","views"));
app.set("view engine", "pug");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(body_parser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));
app.use(hateoasLinker);

// Define routes using URL path

app.use("/", routes);
app.use("/API/v1", apiRoutes)
app.use(handlerError);

/*Socket functions */
io.on("connection", (socket) => {
  console.log("conectado");
  socket.user = "Erik";
  // Canal por defecto
  var canal = "all";
  socket.join(canal);
  socket.on("newMsg", (data) => {
    console.log(data);
    if (canal == "all") {
      socket.broadcast.emit("newMsg", data);
    } else {
    socket.broadcast.to(canal).emit("newMsg", data);
    }
  })
  // Al cambiar de canal
  socket.on("changeChannel", function(newCanal) {
    socket.leave(canal);
    socket.join(newCanal);
    canal = newCanal;
    socket.emit("changeChannel", newCanal);
  })
})
module.exports = app;
